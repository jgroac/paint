export class Pencil {

  constructor(context) {
    this._ctx = context;
  }

  startToDraw(x, y) {
    this._ctx.beginPath();
    this._ctx.moveTo(x, y);
  }

  drawLine(x, y) {
    if (this._lock) {
      return;
    }

    this._ctx.lineTo(x, y);
    this._ctx.stroke();
  }
}
