
const getDistance = (initialPos, finalPos) => {
  const d = finalPos - initialPos;
  return Math.sqrt(d * d);
};

const getIlussionSizeByPosition = (mousePos, rectanglePos) => {
  const width = getDistance(rectanglePos.x, mousePos.x);
  const height = getDistance(rectanglePos.y, mousePos.y);

  return { width, height };
};

const createIlussion = (position, color) => {
  const rectangle = document.createElement('span');
  rectangle.style.position = 'absolute';
  rectangle.style.background = color;
  rectangle.style.left = `${position.x}px`;
  rectangle.style.top = `${position.y}px`;
  rectangle.style.margin = '0px';
  rectangle.style.width = '1px';
  rectangle.style.height = '1px';
  document.body.appendChild(rectangle);
  return { rectangle, position: { x: position.x, y: position.y } };
};


export class Rectangle {

  constructor(context, mouse) {
    this._ctx = context;
    this._Ilussion = createIlussion(mouse, context.fillStyle);
  }


  holdIlussion(x, y) {
    if (!this._Ilussion.rectangle) {
      return;
    }

    const size = getIlussionSizeByPosition({ x, y }, this._Ilussion.position);
    this._Ilussion.rectangle.style.width = `${size.width}px`;
    this._Ilussion.rectangle.style.height = `${size.height}px`;
    /*
     When final mouse position is minor than initial mouse position
     the rectangle position has to be set to  final mouse position to
     show the paint app effect
    */
    if (x < this._Ilussion.position.x) {
      this._Ilussion.rectangle.style.left = `${ x }px`;
    }
    if (y < this._Ilussion.position.y) {
      this._Ilussion.rectangle.style.top = `${ y }px`;
    }
  }

  removeIlussion() {
    this._Ilussion.rectangle.remove();
    this._Ilussion.rectangle = null;
  }

  paint() {
    const elementPos = this._Ilussion.rectangle.getBoundingClientRect();
    const { left, top } = elementPos;
    this._ctx.fillRect(left, top, this._Ilussion.rectangle.offsetWidth,
                                  this._Ilussion.rectangle.offsetHeight);

    this._ctx.stroke();
    this.removeIlussion();
  }
}
