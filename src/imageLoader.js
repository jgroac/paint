
export function loadImageURL(context, url) {
  const image = document.createElement('img');
  image.addEventListener('load', () => {
    context.drawImage(image, 0, 0);
  });
  image.src = url;
}
