export class ColorPicker {
  constructor(context) {
    this._ctx = context;
  }

  getPixelColor(x, y) {
    const pixel = this._ctx.getImageData(x, y, 1, 1);
    const [red, green, blue] = pixel.data;
    return `${red.toString(16)}${green.toString(16)}${blue.toString(16)}`;
  }

}
