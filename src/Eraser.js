export class Eraser {

  constructor(context) {
    this._ctx = context;
  }

  erase(x, y) {
    const size = 10 / 2;
    this._ctx.clearRect(x - size, y - size, size, size);
  }
}
