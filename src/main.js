import { CanvasControls } from './CanvasControls';
import { loadImageURL } from './imageLoader';
import { Mouse } from './Mouse';
import { Tools } from './Tools';


window.onload = () => {
  const canvas = document.querySelector('canvas');
  const controls = new CanvasControls(canvas.getContext('2d'));
  const tools = new Tools(controls.context);
  const mouse = new Mouse();
  const actions = tools.getActions();
  let tool = 'RECTANGLE';
  canvas.style.cursor = 'crosshair';
  canvas.width = document.querySelector('.canvas-box').offsetWidth;
  canvas.height = document.querySelector('.canvas-box').offsetHeight;
  controls.setOptions({ color: '#008B8B', LineCap: 'butt', LineWidth: 1 });
  let actionToRun;
  let mousePressed = false;

  canvas.addEventListener('mousemove', (event) => {
    event.preventDefault();
    mouse.x = Math.floor(event.clientX - canvas.getBoundingClientRect().left);
    mouse.y = Math.floor(event.clientY - canvas.getBoundingClientRect().top);
  });

  canvas.addEventListener('mousedown', (event) => {
    event.preventDefault();
    mousePressed = true;
    actionToRun = actions[tool].mousedown;
    if (actionToRun) {
      actionToRun(mouse);
    }

    canvas.addEventListener('mousemove', (e) => {
      e.preventDefault();
      actionToRun = actions[tool].mousemove;
      if (actionToRun && mousePressed) {
        actionToRun(mouse);
      }
    });
  });

  document.body.addEventListener('mouseup', () => {
    if (event.target.nodeName === 'CANVAS' || event.target.nodeName === 'SPAN') {
      mousePressed = false;
      actionToRun = actions[tool].mouseup;
      if (actionToRun) {
        actionToRun(mouse);
      }
    }
  });

  document.body.addEventListener('submit', (event) => {
    event.preventDefault();
    const element = document.querySelector('#imgURL');
    loadImageURL(controls.context, element.value);
    element.value = '';
  });

  document.querySelector('#tools').addEventListener('change', (event)=>{
    event.preventDefault();
    tool = event.target.options[event.target.selectedIndex].value.toUpperCase();
  });

  const fileInput = document.getElementById('loadfromDisk');
  fileInput.addEventListener('change', (event) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      loadImageURL(controls.context, reader.result);
    });

    reader.readAsDataURL(event.target.files[0]);
  });
};
