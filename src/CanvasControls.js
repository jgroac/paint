const LineCap = ['round', 'butt', 'square'];

export class CanvasControls {

  constructor(context) {
    this._ctx = context;
  }

  get context() {
    return this._ctx;
  }

  get color() {
    return this._ctx.fillStyle;
  }

  setOptions(opts) {
    this.setLineCap(opts.LineCap);
    this.setColor(opts.color);
    this.setLineWidth(opts.LineWidth);
  }

  setColor(color) {
    this._ctx.strokeStyle = color;
    this._ctx.fillStyle = color;
  }

  setLineCap(cap) {
    const self = this;
    LineCap.forEach((element)=> {
      if (element === cap.toLowerCase()) {
        self._ctx.LineCap = element;
        return;
      }
    });
  }


  setLineWidth(width) {
    if (!isNaN(width) && isFinite(width)) {
      this._ctx.lineWidth = width;
    } else {
      throw new Error(' The width should be a number');
    }
  }
}
