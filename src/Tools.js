import { Pencil } from './Pencil';
import { Spray } from './Spray';
import { ColorPicker } from './ColorPicker';
import { Rectangle } from './Rectangle';
import { Eraser } from './Eraser';


let sprayInterval;
export class Tools {
  constructor(context) {
    this.context = context;
    this.pencil = new Pencil(context);
    this.spray = new Spray(context);
    this.colorpicker = new ColorPicker(context);
    this.eraser = new Eraser(context);
    this.actions = {
      PENCIL: {
        mousedown: (pos) => {this.pencil.startToDraw(pos.x, pos.y);},
        mousemove: (pos) => {this.pencil.drawLine(pos.x, pos.y);},
      },
      SPRAY: {
        mousedown: (pos) => {
          sprayInterval = setInterval(() => {
            this.spray.paint(pos.x, pos.y);
          }, 35);
        },
        mouseup: () => {
          clearInterval(sprayInterval);
        },
      },
      COLORPICKER: {
        mousedown: (pos) => {console.log(this.colorpicker.getPixelColor(pos.x, pos.y));},
      },
      ERASER: {
        mousedown: (pos) => {this.eraser.erase(pos.x, pos.y);},
        mousemove: (pos) => {this.eraser.erase(pos.x, pos.y);},
      },
      RECTANGLE: {
        mousedown: (pos) => {this.rectangle = new Rectangle(context, pos);},
        mousemove: (pos) => {this.rectangle.holdIlussion(pos.x, pos.y);},
        mouseup: () => {this.rectangle.paint();},
      },
    };
  }

  getActions() {return this.actions;}

}
