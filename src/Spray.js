const randomPointInRadius = (radius) => {
  for (;;) {
    const x = Math.random() * 2 - 1;
    const y = Math.random() * 2 - 1;
    if (x * x + y * y <= 1) {
      return { x: x * radius, y: y * radius };
    }
  }
};

export class Spray {

  constructor(context) {
    this._ctx = context;
    this._radius = 10;
  }

  paint(centerX, centerY) {
    const area = this._radius * this._radius * 2 * Math.PI;
    const hardness = area / 18;
    for (let i = 0; i < hardness; i++) {
      const point = randomPointInRadius(this._radius);
      this.drawDot(point.x + centerX, point.y + centerY);
    }
  }

  drawDot(x, y) {
    this._ctx.fillRect(x, y, 1, 1);
  }

  set radius(value) {
    this._radius = value;
  }

}
