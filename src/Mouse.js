export class Mouse {
  constructor() {
    this._x = 0;
    this._y = 0;
  }

  set x(value) { this._x = value; }
  set y(value) { this._y = value; }
  get x() { return this._x; }
  get y() { return this._y; }
}
