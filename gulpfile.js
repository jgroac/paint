var gulp = require('gulp');
var babel = require('gulp-babel');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var clean = require('gulp-clean');
var stylus = require('gulp-stylus');


gulp.task('es6toes5',['cleanup'],function () {
  return gulp.src('./src/**/*.js')
    .pipe(babel())
    .pipe(gulp.dest('./build'));
});

gulp.task('cleanup',function () {
  return gulp.src('./build/**/*.js', {read: false})
    .pipe(clean());
});

gulp.task('build',['es6toes5'], function () {
  return browserify('./build/main.js')
  .bundle()
  .pipe(source('bundle.js'))
  .pipe(gulp.dest('./build/'));
});

gulp.task('styles',function () {
  gulp.src('./css/main.styl')
  .pipe(stylus({
    compress: true
  }))
  .pipe(gulp.dest('./css/build'));
});

gulp.task('watch',function () {
  gulp.watch(['src/**/*.js', 'css/main.styl'], ['build', 'styles']);
});

gulp.task('default',['watch']);
